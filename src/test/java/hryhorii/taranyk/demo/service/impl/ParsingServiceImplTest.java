package hryhorii.taranyk.demo.service.impl;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.junit.Test;

import hryhorii.taranyk.demo.model.Bank;

public class ParsingServiceImplTest {

	ParsingServiceImpl parsingServiceImpl = new ParsingServiceImpl();

	@Test(expected = FileNotFoundException.class)
	public void WhenFileIsNullExceptionIsThrown() throws FileNotFoundException, IOException, ParseException {
		parsingServiceImpl.parse(null);
	}

	@Test
	public void testWhenParseJsonInput() throws FileNotFoundException, IOException, ParseException {
		File file = new File("file.JSON");
		Bank bank = parsingServiceImpl.parse(file);

		assertEquals("file", bank.getName());
		assertEquals("USD", bank.getCurrencies().get(0).getCurrencyName());
		assertEquals("25.90", bank.getCurrencies().get(0).getBuyRate());
		assertEquals("26.10", bank.getCurrencies().get(0).getSellRate());
		assertEquals("EUR", bank.getCurrencies().get(1).getCurrencyName());
		assertEquals("30.00", bank.getCurrencies().get(1).getBuyRate());
		assertEquals("31.00", bank.getCurrencies().get(1).getSellRate());
		assertEquals("CHF", bank.getCurrencies().get(2).getCurrencyName());
		assertEquals("26.00", bank.getCurrencies().get(2).getBuyRate());
		assertEquals("-", bank.getCurrencies().get(2).getSellRate());

	}

	@Test
	public void testWhenParseCSVInpun() throws FileNotFoundException, IOException, ParseException {

		File file = new File("file.CSV");
		Bank bank = parsingServiceImpl.parse(file);

		assertEquals("file", bank.getName());
		assertEquals("USD", bank.getCurrencies().get(0).getCurrencyName());
		assertEquals("25.90", bank.getCurrencies().get(0).getBuyRate());
		assertEquals("25.98", bank.getCurrencies().get(0).getSellRate());
		assertEquals("EUR", bank.getCurrencies().get(1).getCurrencyName());
		assertEquals("30.00", bank.getCurrencies().get(1).getBuyRate());
		assertEquals("-", bank.getCurrencies().get(1).getSellRate());
		assertEquals("CHF", bank.getCurrencies().get(2).getCurrencyName());
		assertEquals("-", bank.getCurrencies().get(2).getBuyRate());
		assertEquals("26.74", bank.getCurrencies().get(2).getSellRate());

	}

	@Test
	public void testWhenParseXMLInpun() throws FileNotFoundException, IOException, ParseException {

		File file = new File("file.XML");
		Bank bank = parsingServiceImpl.parse(file);

		assertEquals("file", bank.getName());
		assertEquals("USD", bank.getCurrencies().get(0).getCurrencyName());
		assertEquals("25.90", bank.getCurrencies().get(0).getBuyRate());
		assertEquals("26.10", bank.getCurrencies().get(0).getSellRate());
		assertEquals("EUR", bank.getCurrencies().get(1).getCurrencyName());
		assertEquals("30.00", bank.getCurrencies().get(1).getBuyRate());
		assertEquals("31.00", bank.getCurrencies().get(1).getSellRate());
		assertEquals("RUB", bank.getCurrencies().get(2).getCurrencyName());
		assertEquals("0.420", bank.getCurrencies().get(2).getBuyRate());
		assertEquals("0.426", bank.getCurrencies().get(2).getSellRate());

	}

}
