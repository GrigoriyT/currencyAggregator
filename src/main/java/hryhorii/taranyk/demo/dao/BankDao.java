package hryhorii.taranyk.demo.dao;

import java.util.Optional;

import hryhorii.taranyk.demo.model.Bank;

public interface BankDao {
	Bank save(Bank bank);

	Optional<Bank> findByName(String name);

	boolean delete(Bank bank);
}
