package hryhorii.taranyk.demo.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;

import hryhorii.taranyk.demo.model.Bank;

public interface ParsingService {

	Bank parse(File file) throws FileNotFoundException, IOException, ParseException;

}
