package hryhorii.taranyk.demo.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import hryhorii.taranyk.demo.model.Bank;
import hryhorii.taranyk.demo.model.Currency;
import hryhorii.taranyk.demo.service.ParsingService;

public class ParsingServiceImpl implements ParsingService {

	@Override
	public Bank parse(File file) throws FileNotFoundException, IOException, ParseException {
		if (file == null) {
			throw new FileNotFoundException();
		}

		String fileName = file.getName();
		int pointIndex = fileName.lastIndexOf(".");
		String bankName = fileName.substring(0, pointIndex);
		String fileFormat = fileName.substring(pointIndex + 1, fileName.length());
		Bank bank = new Bank();
		bank.setName(bankName);
		
		if ("JSON".equalsIgnoreCase(fileFormat)) {
			bank.setCurrencies(readFromJson(fileName));

		} else if ("XML".equalsIgnoreCase(fileFormat)) {
			bank.setCurrencies(readFromXML(fileName));

		} else if ("CSV".equalsIgnoreCase(fileFormat)) {
			bank.setCurrencies(readFromCSV(fileName));

		}

		return bank;
	}

	private List<Currency> readFromJson(String fileName) throws FileNotFoundException, IOException, ParseException {
		JSONParser parser = new JSONParser();
		JSONArray array = (JSONArray) parser.parse(new FileReader(fileName));
		ArrayList<Currency> currencyList = new ArrayList<>();

		for (Object elem : array) {
			Currency currency = new Currency();
			JSONObject object = (JSONObject) elem;

			String currencyName = (String) object.get("currencyName");
			currency.setCurrencyName(currencyName);
			String buyRate = (String) object.get("buyRate");
			currency.setBuyRate(buyRate);
			String sellRate = (String) object.get("sellRate");
			currency.setSellRate(sellRate);

			currencyList.add(currency);

		}
		return currencyList;
	}

	private List<Currency> readFromCSV(String fileName) {
		BufferedReader bReader = null;
		String line = "";
		String separator = "\\s";
		ArrayList<Currency> currencyList = new ArrayList<>();

		try {
			bReader = new BufferedReader(new FileReader(fileName));

			while ((line = bReader.readLine()) != null) {
				String[] currencyArray = line.split(separator);
				Currency currency = new Currency();

				for (int i = 0; i < currencyArray.length; i++) {
					System.out.print(currencyArray[i] + " ");
					if (i == 0) {
						currency.setCurrencyName(currencyArray[i]);
					} else if (i == 1) {
						currency.setBuyRate(currencyArray[i]);
					} else if (i == 2) {
						currency.setSellRate(currencyArray[i]);
					}
				}
				currencyList.add(currency);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (bReader != null) {
				try {
					bReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return currencyList;

	}

	private List<Currency> readFromXML(String fileName) {
		ArrayList<Currency> currencyList = new ArrayList<>();
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(fileName);

			NodeList nodeList = doc.getElementsByTagName("elem");

			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				Currency currency = new Currency();

				if (Node.ELEMENT_NODE == node.getNodeType()) {
					Element element = (Element) node;
					currency.setCurrencyName(element.getElementsByTagName("currencyName").item(0).getTextContent());
					currency.setBuyRate(
							element.getElementsByTagName("buyRate").item(0).getTextContent());
					currency.setSellRate(
							element.getElementsByTagName("sellRate").item(0).getTextContent());
				}
				currencyList.add(currency);
			}
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		return currencyList;

	}
}