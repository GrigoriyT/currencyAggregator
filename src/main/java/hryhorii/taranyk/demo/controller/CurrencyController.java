package hryhorii.taranyk.demo.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import hryhorii.taranyk.demo.model.Bank;
import hryhorii.taranyk.demo.service.CurrencyService;
import hryhorii.taranyk.demo.service.ParsingService;

@RestController
@RequestMapping(value = "/api/aggregator")
public class CurrencyController {

	private final ParsingService parsingService;
	private final CurrencyService currencyService;

	@Autowired
	public CurrencyController(ParsingService parsingService, CurrencyService currencyService) {
		this.parsingService = parsingService;
		this.currencyService = currencyService;
	}

	@GetMapping("/hello/{name}")
	public String hello(@PathVariable("name") String name) {
		System.out.println("method hello was invoked!");
		return "Hello " + name;
	}

	@GetMapping("/bestprice")
	public String bestprices() {
		System.out.println("method bestprice was invoked!");
		// currencyService.equals()
		return "Best Price";
	}

	@PostMapping(value = "/file/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public void upload(@RequestParam("file") MultipartFile multipartFile) throws IOException {
		if (multipartFile == null) {
			System.out.println("Not found");

		} else {
			File file = convert(multipartFile);
			System.out.println("Files " + multipartFile.getOriginalFilename() + " exits: " + file.exists());
			System.out.println(multipartFile.getOriginalFilename());
			try {
				Bank bank = parsingService.parse(file);
			} catch (ParseException e) {
				e.printStackTrace();
			}

		}
	}

	@GetMapping("/currencies/{bank}")
	@ResponseBody
	// public List<Currency> currencies(@PathVariable String bank) {
	// return Collections.emptyList();
	// return currencyService.fi
	// }

	public static File convert(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}

}
